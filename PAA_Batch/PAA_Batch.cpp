// PAA_Batch.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "PDTW_Batch.h"
#include <iostream>
#include <fstream>
#include <string>

int main(void)
{
	char Dataset[] = R"(C:\Datasets\ItalyPowerN.txt)";
	char QueryTmpl[] = R"(C:\Datasets\ItalyPower\Inside\InsideQuery)";
	int QueryStartIdx = 0;
	int QueryEndIdx = 9;
	char DL = ',';
	char OutputFileAddr[] = R"(C:\Datasets\ItalyPower\Inside\ItalyPower_PAA_IQ.csv)";






	// ------------------------ INITIALIZATION ----------------------- // 
	std::ofstream hOutputFile;
	Output MyOutput;

	std::string DatasetPAA = getPAAFilename(std::string(Dataset));
	// ------------------------ OFFLINE RUN ----------------------- // 
	char **argv = new char*[4];
	argv[0] = "Null";
	argv[1] = "-PAA";
	argv[2] = Dataset;
	argv[3] = "2";

	main_aux(argv, MyOutput);


	// ------------------------ ONLINE RUN ----------------------- // 
	char QueryFile[256];
	char QueryFilePAA[256];
	hOutputFile.open(OutputFileAddr);

	if (hOutputFile.is_open() == false)
	{
		std::cout << "Error: Error opening output file" << std::endl;
		abort();
	}
	hOutputFile << "Index" << DL << "PAA  TSID" << DL << "PAA TStart" << DL << "PAA TEnd" << DL << "PAA Corr" << DL << "PAA Time(s)" << std::endl;


	for (int k = QueryStartIdx; k <= QueryEndIdx; k++)
	{
		// Generate PAA file
		sprintf_s(QueryFile, "%s%d.txt", QueryTmpl, k);
		argv[1] = "-PAA";
		argv[2] = QueryFile;
		argv[3] = "2";
		main_aux(argv, MyOutput);

		// Resetting output
		MyOutput = Output();

		// Compute the correlation
		sprintf_s(QueryFilePAA, "%s%d_PAA.txt", QueryTmpl, k);
		argv[1] = "-DTW";
		argv[2] = (char *)DatasetPAA.c_str();
		argv[3] = QueryFilePAA;
		main_aux(argv, MyOutput);


		hOutputFile << k << DL << MyOutput.TSId << DL << MyOutput.TSStart << DL << MyOutput.TSEnd << DL << MyOutput.Corr << DL << MyOutput.Time << std::endl;

	}

	hOutputFile.close();
	delete[] argv;

	std::cout << "*********************************************" << std::endl;
	std::cout << "Task Completed" << std::endl;

	getchar();
    return 0;
}

