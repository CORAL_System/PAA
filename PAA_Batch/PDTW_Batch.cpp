#include "stdafx.h"
#include <fstream>
#include <istream>
#include <string>
#include <vector>
#include <numeric>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <math.h>
#include <limits> // std::numeric_limits<double>::max();
#include <algorithm> // min()
#include <errno.h>
#include <cstring> // strerror()
#include <time.h>
#include <ios>
#include "PDTW_Batch.h"

#define MUZDEBUG 0

double distFuncM(double x, double y)
{

        return fabs(x - y);

}
double distFuncC(double x, double y)
{

        return fabs(x - y);

}


// FOR DTW TAKE BIN SIZE AND RUN PAA ON QUERY FILE
void usage() {
    std::cout << "use this program in one of the following ways" << std::endl;
    std::cout << "./PDTW -PAA file blockSize" << std::endl;
    std::cout << "./PDTW -DTW dataFile queryFile" << std::endl;
    std::cout << "./PDTW -OUTLIERS dataFile startLoc endLoc" << std::endl;
}

struct DTWData {
    double distance;
    int timeSeries;
    int locStart;
    int blkSize;
    DTWData(double in_distance, int in_timeSeries, int in_locStart, int in_blkSize) {
        distance = in_distance;
        timeSeries = in_timeSeries;
        locStart = in_locStart;
        blkSize = in_blkSize;
    }

    DTWData() {
        distance = 0;
        timeSeries = 0;
        locStart = 0;
        blkSize = 0;
    }

    bool empty() {
        return (distance == 0) && (timeSeries == 0) && (locStart == 0) && (blkSize == 0);
    }
};

std::ostream& operator<<(std::ostream& os, DTWData& d) {
    os << "Time Series: " << d.timeSeries <<"["<<d.locStart<<"]"<< std::endl;
    os << "Location: " << d.locStart << " to " << d.locStart+d.blkSize << std::endl;
    os << "Correlation: " << d.distance << std::endl;
    return os;
}

// takes the given file and adds '_PAA' to the end
// returns the new filepath
std::string createPAAFilepath(std::string in) {
    std::size_t point = in.find(".");
    if (point == std::string::npos)
        return in + "_PAA";
    return in.substr(0, point) + "_PAA" + in.substr(point);
}

std::string getPAAFilename(std::string incoming) {
    if (incoming.find("_PAA") != std::string::npos) { // found "_PAA"
        return incoming;
    }
    return createPAAFilepath(incoming);
}

//simple Euclidean distance between two points
double distFunc(double x, double y) {
    return (pow((x - y), 2));
}

//DTW calculation between two vectors. one is query and other Data sequence
double simpleDTW(const std::vector<double>& t1, const std::vector<double>& t2) {
/*
    int m = t1.size();
    int n = t2.size();
    int len=std::max(t1.size(),t2.size());
    // create cost matrix
    double cost[m][n];
    cost[0][0] = distFunc(t1[0], t2[0]);

    // calculate first row
    for(int i = 1; i < m; i++) {
        cost[i][0] = cost[i-1][0] + distFunc(t1[i], t2[0]);
    }
    // calculate first column
    for(int j = 1; j < n; j++) {
        cost[0][j] = cost[0][j-1] + distFunc(t1[0], t2[j]);
    }
    // fill matrix
    for(int i = 1; i < m; i++) {
        for(int j = 1; j < n; j++) {
            cost[i][j] = distFunc(t1[i],t2[j])+ std::min(cost[i-1][j],std::min(cost[i][j-1], cost[i-1][j-1]));
        }
    }
    
    //return sqrt(cost[m-1][n-1]);
    //return normalized dtw distance
    return (sqrt(cost[m-1][n-1]))/(2*len);
	*/
	return 1;
}
void znormSequenceS(std::vector<double> &t1)
{
    double gMean=0;   //mean of the sequences
    double gStd=0;     //std of sequences
    double sum=0;


    gStd=0;
    sum=0;
    gMean=0;
    //calculate sum of sequence
    for(int j=0;j<t1.size();j++)
    {
        sum+=t1[j];
    }
    //calculating mean of this sequence
    gMean=sum/t1.size();
    //calculating std of these sequences
    for(int j=0;j<t1.size();j++)
    {
        gStd+=pow(t1[j]-gMean,2);
    }
    gStd=sqrt((gStd/t1.size()));
	if (gStd == 0)		//if std is 0 set it to ver small number to avoid division by 0.
		gStd = std::numeric_limits<double>::epsilon();
    //z-normalize this sequence

    for(int j=0;j<t1.size();j++)
    {
        t1[j]=(t1[j]-gMean)/gStd;

    }

}

double correlation(std::vector<double>& t1,std::vector<double>& t2)
{
    znormSequenceS(t1);
    znormSequenceS(t2);

    double corr=0;

    //calculate correlation measure

    for(int i =0;i<t1.size();i++)
    {
        double term=t1[i]*t2[i];
        corr=corr+ term;
    }
    corr=corr/t1.size();
    return corr;
    //OLD CORRELATION CODE
    /*
    double meanX=0;
    double meanY=0;
    double stdX=0;
    double stdY=0;
    double corr=0;

    meanX=std::accumulate( t1.begin(), t1.end(), 0.0)/t1.size();
    meanY=std::accumulate( t2.begin(), t2.end(), 0.0)/t2.size();
    //calculate standard deviations
    for(int i=0;i<t1.size();i++)
        stdX+=pow(t1[i]-meanX,2);
    stdX=sqrt(stdX/t1.size());
    for(int j=0;j<t2.size();j++)
        stdY+=pow(t2[j]-meanY,2);
    stdY=sqrt(stdY/t2.size());

    //calculate correlation measure

    for(int i =0;i<t1.size();i++)
    {
        double term1=(t1[i]-meanX)/stdX;
        double term2=(t2[i]-meanY)/stdY;
        double term3=term1*term2;
        corr=corr+ term3;
    }
    corr=corr/t1.size();
    return corr;
    */

}
// both incoming files should be PAA'd already
DTWData DTWaFile(std::string dataFile, std::string queryFile, int method) {
    std::ifstream data(getPAAFilename(dataFile).c_str());
    std::ifstream query(getPAAFilename(queryFile).c_str());

    if (!data) {
            std::cout << "ERROR: ifstream failed on " << dataFile << ": " << std::endl;
            return DTWData();
    }
    if (!query) {
            std::cout << "ERROR: ifstream failed on " << queryFile << ": "<< std::endl;
            return DTWData();
    }
	clock_t start;
	start = clock();
    std::string dataTimePoints;
    std::string queryData;

    std::vector<double> dataVector;
    std::vector<double> queryVector;
    int curTimeSeries = 0;
    int bestMatchTimeSeries = 1;
    int bestMatchIdx = 1;
    int bestMatchBlkSz = 2;
    double bestMatchDistance =0;// std::numeric_limits<double>::max();

    // turn query into vector
    std::getline(query, queryData);
    std::size_t prev = 0, pos;
    while ((pos = queryData.find_first_of(" ,", prev)) != std::string::npos) {
        if (pos > prev)
            queryVector.push_back(stod(queryData.substr(prev, pos-prev)));
        prev = pos+1;
    }
    if (prev < queryData.length())
        queryVector.push_back(stod(queryData.substr(prev, std::string::npos)));
        
    while (dataTimePoints.empty() && data.good())
        std::getline(data, dataTimePoints);

    while (data.good()) {
        // get the next data timeseries
        curTimeSeries++;        
        // split the timeseries numbers on space or comma
        std::size_t prev = 0, pos;
        while ((pos = dataTimePoints.find_first_of(" ,", prev)) != std::string::npos) {
            if (pos > prev)
                dataVector.push_back(stod(dataTimePoints.substr(prev, pos-prev)));
            prev = pos+1;
        }
        if (prev < dataTimePoints.length())
            dataVector.push_back(stod(dataTimePoints.substr(prev, std::string::npos)));

        double newBest;
        // run through all combinations from query
        for (int blkSz = 2; blkSz <= (int)dataVector.size(); blkSz++) {
            for (int startIdx = 0; startIdx+blkSz <= (int)dataVector.size(); startIdx++) {
                std::vector<double> subVec(dataVector.begin()+startIdx, dataVector.begin()+startIdx+blkSz);

                if(queryVector.size()==subVec.size())       //both vectors have same size then compute correlation
                {
                    newBest = correlation(subVec,queryVector);
                    //newBest=newBest;     //normalize it

                    if (fabs(newBest) >= fabs(bestMatchDistance)) {
                                bestMatchDistance = newBest;
                                bestMatchIdx = startIdx;
                                bestMatchBlkSz = blkSz;
                                bestMatchTimeSeries = curTimeSeries;
                    }
                }
            }
        }
        
        // get the next data timeseries
        dataVector.clear();
        dataTimePoints = "";
        while (dataTimePoints.empty() && data.good())
            std::getline(data, dataTimePoints);
    }
	//std::cout << "PAAB took: " << (clock() - start) / double(CLOCKS_PER_SEC) << "s]" << std::endl;
    return DTWData(bestMatchDistance, bestMatchTimeSeries, bestMatchIdx, bestMatchBlkSz);
}


// how big will the incoming numbers be?
double PAA(std::vector<double> inData) {
    double ave = 0;
    for (int i=0; i< (int)inData.size(); i++) {
            ave += inData[i];
    }
    ave /= (int)inData.size();
    return ave;
}

// runs through the given file and calculates PAA with a N points in each block
std::string PAAaFile(std::string inData, int N) {
    if (N <= 0) {
        std::cout << "ERROR: block size is " << N << ": should be greater than 0" << std::endl;
        return "";
    }
    std::ifstream inFile(inData.c_str());
    std::string PAAFile = createPAAFilepath(inData);
    std::ofstream outFile(PAAFile.c_str());
    std::string timePoint;
    std::size_t newLine = std::string::npos;
    std::vector<double> data;
    char delim = ',';

    if (!inFile) {
        std::cout << "ERROR: ifstream failed on " << inData << ": " << std::endl;
        return "";
    }

    int len = inFile.tellg();
    std::getline(inFile, timePoint);
    if (timePoint.find(",") == std::string::npos) { // no commas
        delim = ' ';
    }
    inFile.seekg(len, std::ios_base::beg);

    while (inFile.good()) {
        // run until N time points are grabbed OR
        // the end of the time series is hit
        for (int i=0; (i < N) && (newLine == std::string::npos); i++ ) {
            //if data has N points in it should break
            if(data.size()==N)
                break;
            // get one time value
            std::getline(inFile, timePoint, delim);
            // check if we hit the end of the time series
            // ex) "number1\nnumber2"
            newLine = timePoint.find("\n");
            data.push_back(stod(timePoint));
        }

        // send one chunk of data to PAA and write to outFile
        outFile << PAA(data) << " ";
        // clear data to grab the next chunk
        data.clear();

        // if you hit the end of a time series "number1\nnumber2"
        if (newLine != std::string::npos) {
            // timer series break
            outFile << std::endl << std::endl;
            // add the second number to data
            if (timePoint.substr(newLine).size() > 1) {
                    data.push_back(stod(timePoint.substr(newLine)));
            }
            // reset newLine
            newLine = std::string::npos;
        }
    }
    return PAAFile;
}




// CREATE NEW FLAG FOR OUTLIER.
// OUTLIER WILL CHECK A FILE AGAINST ITSELF TO FIND THE MAX DTW DISTANCE
int main_aux(char** argv, Output &_Output) {
    /*
    if (argc != 4 && argc != 3) {
        usage();
        return -1;
    }
    */
 // argv[1]="-PAA";
 // //argv[2]=R"(C:\Datasets\Earthquakes.txt)";
 // argv[2] = R"(C:\Datasets\Earthquakes\Inside\query11.txt)";
 ////argv[2]="C:/Data/wafer/Inside/query10.txt";
 // argv[3]="2";

 //  argv[1]="-DTW";
 //  argv[2]= R"(C:\Datasets\Earthquakes_PAA.txt)";
 //  argv[3]= R"(C:\Datasets\Earthquakes\Inside\query11_PAA.txt)";

#if MUZDEBUG
	for (int k = 0; k < 4; k++)
	{
		printf("%s\n",argv[k]);

	}
#endif



 int method=2;      //for DTW-ED=0,DTW-MD =1 or DTW-CD=2
    clock_t start, stop;

    if (!std::string(argv[1]).compare("-PAA")) {
        start = clock();
        std::string PAAFile = PAAaFile(argv[2], atoi(argv[3]));
        stop = clock();
        if (PAAFile.empty()) {
            std::cout << "PAAaFile failed" << std::endl;
            return -1;
        }
        std::cout << "PAA computed for " << argv[2] << ". Written to " << PAAFile << std::endl;
        int s = (stop - start) / CLOCKS_PER_SEC;
        long t = ((stop - start) % CLOCKS_PER_SEC)/1000;
        //std::cout << "PAA took: " << s << " seconds and " << t << " milliseconds to compute" << std::endl;
        std::cout << "PAAF took: " <<(clock()-start)/double(CLOCKS_PER_SEC) << "s]" << std::endl;
    } else if (!std::string(argv[1]).compare("-DTW")) {
        start = clock();
        DTWData d = DTWaFile(argv[2], argv[3],method);
        stop = clock();
        if (d.empty()) {
            std::cout << "DTWaFile failed" << std::endl;
            return -1;
        }
      //  std::cout << "DTW computed for data " << argv[2] << " and query " << argv[3];
        std::cout << d << std::endl;
		double TimeElasped = (clock() - start) / double(CLOCKS_PER_SEC);			

		std::cout << "PAA took: " << TimeElasped << "s]" << std::endl;
        int s = (stop - start)/ CLOCKS_PER_SEC;
        long t = ((stop - start) % CLOCKS_PER_SEC)/1000;
        //std::cout << "DTW took: " << s << " seconds and " << t << " milliseconds to compute" << std::endl;

		// Saving to output structure
		_Output.TSId = d.timeSeries;
		_Output.TSStart = d.locStart;
		_Output.TSEnd = d.locStart + d.blkSize - 1;
		_Output.Corr = d.distance;
		_Output.Time = TimeElasped;


        
    } else {
        usage();
        return -1;
    }
  //  return 1;
}
