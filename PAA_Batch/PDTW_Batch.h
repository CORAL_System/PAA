
#pragma once

#include <string>

struct Output
{
	int TSId = 0;
	int TSStart = 0;
	int TSEnd = 0;
	double Corr = 0;
	double Time = 0;
	
};


int main_aux(char** argv, Output &_Output);
std::string getPAAFilename(std::string incoming);

